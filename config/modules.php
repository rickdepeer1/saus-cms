<?php return [
    "ScheduleService" => [
        "moduleName" => "schedule",
        "model" => \App\Cms\Schedule\Models\Schedule::class,
        "service" => \App\Cms\Schedule\Services\ScheduleService::class,
        "children" => ["TaskService"],
        "FormController" => ["class" => \App\Schedule\Controllers\ScheduleFormController::class],
        "columnsToDisplay" => [
            'id' => '#',
            'schedule',
            'description',
            'TaskService' => 'Tasks',
            'created_at' => 'Date created',
            'actionButtons' => '',
        ],
    ],
    "TaskService" => [
        "moduleName" => "task",
        "model" => \App\Cms\Schedule\Models\Task::class,
        "service" => \App\Cms\Schedule\Services\TaskService::class,
        "columnsToDisplay" => [
            'id' => '#',
            'task',
            'created_at' => 'Date created',
            'actionButtons' => '',
        ],
    ],
];