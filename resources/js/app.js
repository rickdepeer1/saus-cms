require('./bootstrap');


$(document.body).on("click", ".setting-save", function (e) {
    let module = $(e.currentTarget).data('module');
    let setting_id = $(e.currentTarget).data('id');
    let link = $(e.currentTarget).data('link');
    let setting_key = $("#key-" + link);
    let setting_value = $("#value-" + link);

    console.log(module, setting_id, setting_key.val(), setting_value.val());

   if(setting_key.val() === setting_key.data('original') && setting_value.val() === setting_value.data('original')){
       alert('settings same as original');
   }

    $.post( "/cms/" + module + "/setting/STORE", {'id': setting_id, 'key': setting_key.val(), 'value': setting_value.val()} , function( data ) {
        data = JSON.parse(data);
        window.location.href = data.redirect;
    });

    $('#save-' + link).slideUp(500);
    $('#edit-' + link).delay(500).slideDown(500);
    $('#' + link).attr('disabled', '');

}).on('click', ".setting-edit", function (e) {

    let link = $(e.currentTarget).data('link');
    $('#' + link).removeAttr('disabled');
    $('#edit-' + link).slideUp(500);
    $('#save-' + link).delay(500).slideDown(500);
    console.log($('#edit-' + link), link);
}).on("click", '.setting-add', function (e) {
    let module = $(e.currentTarget).data('module');
    $.get("/cms/" + module + "/setting/ADD" , function( data ) {
        $('.settings-wrapper').append(data);

        let link = $(data).find('.setting-edit').data('link');
        $('#' + link).removeAttr('disabled');
        $('#edit-' + link).slideUp(0);
        $('#save-' + link).slideDown(500);

    });
});