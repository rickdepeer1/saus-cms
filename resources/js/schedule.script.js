
$(document).ready(function () {
    console.log('---- Script loaded ----');

    let tasks = JSON.parse($("#tasks").val());

    UpdateTasks(tasks);


    /****************************************************
     *
     *      All jQuery events in sequence to reduce $ calls
     *
     ***************************************************/
    $(document.body).on('click', '#add-task', function ()
    {
        // retrieve fata form hidden input field to generate display for user
        let id = $('#task-id-input').val();
        let taskTitle = $('#task-input').val();
        let expectedHours = $('#expectedHours-input').val();
        let datetime = $('#datetime-input').val();
        let priority = $('#priority-input').val();
        tasks = JSON.parse($("#tasks").val());

        //set id for new entry
        if(parseInt(id) === 0)
        {
            id = tasks.length + 1;
        }


        //Create task as json, push to all tasks and send to update func
       let task = {"id": parseInt(id),"task": taskTitle, "priority": parseInt(priority), "expectedHours": parseInt(expectedHours), "datetime": datetime};

        tasks.push(task);

        UpdateTasks(tasks);

    }).on('click', '#task-edit', function (e) {

        let tasks = JSON.parse($("#tasks").val());

        let id = parseInt($(e.currentTarget).attr('data-id'));
    }).on('click', '#task-remove', function (e) {

        let tasks = JSON.parse($("#tasks").val());

        let id = parseInt($(e.currentTarget).attr('data-id'));

        for(let task in tasks)
        {
            if(id === (parseInt(task) + 1))
            {
                //tasks.slice(parseInt(task), 1);
                remove(tasks, parseInt(task));
                console.log(task);
            }
        }

        console.log(tasks);
        UpdateTasks(tasks);
    });
});

function UpdateTasks(tasks)
{
    let template = '';
    let priority = 1;

    for(let task in tasks)
    {
        template += "<tr><th scope='row'>"+(parseInt(task)+1)+"</th><td>"+tasks[task].task+"</td><td>"+tasks[task].priority+"</td><td><a href='#' class='pl-3' id='task-up' data-id='\"+(parseInt(task)+1)+\"'><i class='fas fa-caret-up pr-2'></i></a><a href='#' id='task-down' data-id='\"+(parseInt(task)+1)+\"'><i class='fas fa-caret-down pr-2'></i></a></td><td><a href='#' id='task-edit' data-id='"+(parseInt(task)+1)+"'><i class='fas fa-edit pr-2'></i></a><a href='#' id='task-remove' data-id='"+(parseInt(task)+1)+"'><i class='fas fa-minus-circle'></i></a></td></tr>";
        priority = parseInt(task)+2;
    }

    $('#priority-input').val(priority);

    $('#tasks-table tbody').html(template);

    $("#tasks").val(JSON.stringify(tasks));
    console.log('---- Tasks Updated ----');
}

function remove(array, element) {
    array.splice(element, 1);
}