@extends(env("SAUS_LAYOUT"))

@section('content')
    <a href="/cms/products/" class="btn btn-default">Go Back</a>
    <h1>{{$product->product}}</h1>
    <div>
        {!! $product->description !!}
    </div>
    <hr>
    <small>Created on {{$product->created_at}}</small>
    <hr>
    <a href="/cms/products/{{$product->id}}/edit" class="btn btn default">Edit</a>
    {!! Form::open(['action' => ['ProductsController@destroy', $product->id], 'method' => 'POST', 'class' => 'float-right']) !!}
    {{Form::hidden('_method', 'DELETE')}}
    {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
    {!! Form::close() !!}
@endsection
