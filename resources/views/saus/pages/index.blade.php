@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1>Pages</h1>
    <a href="/cms/pages/create" class="btn btn-secondary">Create</a>
    @if(count($pages) > 0)
        <table class="table table-striped mt-3">
            <tr>
                <th>#</th>
                <th>Page</th>
                <th>Description</th>
                <th>Body</th>
                <th></th>
            </tr>
        @foreach($pages as $page)
                <tr>
                    <td>{{$page->id}}</td>
                    <td><b><a href="#">{{$page->page}}</a><br/><small>Created on {{$page->created_at}}</small></b></td>
                    <td>{{ $page->description }}</td>
                    <td>{{ $page->body }}</td>
                    <td><a class="p-2" href="/cms/pages/{{$page->id}}/edit"><i class="fas fa-edit"></i></a>
                        {!! Form::open(['action' => ['PagesController@destroy', $page->id], 'method' => 'POST', 'class' => 'inline-form']) !!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::button(' <i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'link-btn'))}}
                        {!! Form::close() !!}</td>
                </tr>
        @endforeach
        </table>
        {{$pages->links()}}
    @else
        <p>No Pages found</p>
    @endif
@endsection