@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1>Edit Product</h1>
    {!! Form::open(['action' => ['ProductsController@update', $product->id], 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('product', 'Product Title')}}
        {{Form::text('product', $product->product, ['class' => 'form-control', 'placeholder' => 'Title'])}}
    </div>
    <div class="form-group">
        {{Form::label('description', 'Description')}}
        {{Form::textarea('description', $product->description, ['id' => 'ckeditor', 'class' => 'form-control', 'placeholder' => 'Product description'])}}
    </div>
    <div class="form-group">
        {{Form::label('price', 'Price')}}
        {{Form::text('price', $product->price, ['class' => 'form-control', 'placeholder' => '0.0'])}}
    </div>
    <div class="form-group">
        {{Form::label('stock', 'Stock')}}
        {{Form::text('stock', $product->stock, ['class' => 'form-control', 'placeholder' => '0'])}}
    </div>
    <div class="form-group">
        {{Form::label('sold', 'Sold')}}
        {{Form::text('sold', $product->sold, ['class' => 'form-control', 'placeholder' => '0'])}}
    </div>
    <a href="/cms/products" class="btn btn-default">Back</a>
    {{Form::hidden('_method', 'PUT')}}
    {{Form::submit('Submit', ['class' => 'btn btn-secondary float-right'])}}
    {!! Form::close() !!}
@endsection
