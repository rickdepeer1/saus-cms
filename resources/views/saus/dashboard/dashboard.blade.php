@extends(env("SAUS_LAYOUT"))

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        You are logged in!
                        <hr>
                        <a class="btn btn-secondary" href="/cms/posts/create/">Make Post</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
