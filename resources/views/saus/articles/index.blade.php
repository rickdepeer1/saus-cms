@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1>Articles</h1>
    <a href="/cms/posts/create" class="btn btn-secondary">Create</a>
    <hr>
    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div>
                <h3><a href="/cms/posts/{{$post->id}}">{{$post->title}}</a></h3>
                <small>Written on {{$post->created_at}}</small>
            </div>
            <hr>
            @endforeach
            {{$posts->links()}}
    @else
        <p>No posts found</p>
    @endif
@endsection
