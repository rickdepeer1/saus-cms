@extends(env("SAUS_LAYOUT"))
@section('content')
    <h1>Create Post</h1>
    {!! Form::open(['action' => 'PostsController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('title', 'Tile')}}
            {{Form::text('title', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        <div class="form-group">
            {{Form::label('body', 'Body')}}
            {{Form::textarea('body', '', ['id' => 'ckeditor', 'class' => 'form-control', 'placeholder' => 'Body text'])}}
        </div>
        <a href="/posts" class="btn btn-default">Back</a>
        {{Form::submit('Submit', ['class' => 'btn btn-secondary float-right'])}}
    {!! Form::close() !!}
@endsection
