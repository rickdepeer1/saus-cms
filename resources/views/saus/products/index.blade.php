@extends(env("SAUS_LAYOUT"))
@section('content')
    <h1>Products</h1>
    <a href="/cms/products/create" class="btn btn-secondary">Create</a>
    @if(count($products) > 0)
        <table class="table table-striped mt-3">
            <tr>
                <th>#</th>
                <th>Product</th>
                <th>Description</th>
                <th>Price</th>
                <th>Stock</th>
                <th>Sold</th>
                <th></th>
            </tr>
        @foreach($products as $product)
            <tr>
                <td>{{$product->id}}</td>
                <td><b><a href="/cms/products/{{$product->id}}">{{$product->product}}</a><br/><small>Created on {{$product->created_at}}</small></b></td>
                <td>{!! $product->description !!}</td>
                <td>&#8364; {{number_format($product->price, 2, '.', '')}}</td>
                <td>{{$product->stock}}</td>
                <td>{{$product->sold}}</td>
                <td><a class="p-2" href="/cms/products/{{$product->id}}/edit"><i class="fas fa-edit"></i></a>
                    {!! Form::open(['action' => ['ProductsController@destroy', $product->id], 'method' => 'POST', 'class' => 'inline-form']) !!}
                    {{Form::hidden('_method', 'DELETE')}}
                    {{Form::button(' <i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'link-btn'))}}
                    {!! Form::close() !!}</td>
            </tr>
        @endforeach
        </table>
        {{$products->links()}}
    @else
        <p>No Products found</p>
    @endif
@endsection