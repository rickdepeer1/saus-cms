<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Saus CMS') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    @stack('styles')

    <!-- Font Awesome icons -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
</head>
<body>
<div id="app">
    @include('saus._default.components.navbar')
    <div class="container-fluid">
        <div class="row">
            <div id="sidebar" class="col-2 border-right pt-3">
                @guest
                @else
                    @include('saus._default.components.sidebar')
                @endguest
            </div>
            <div id="main-content" class="col-7 pt-3">
                @include('saus._default.components.messages')
                @yield('content')
            </div>
            <div id="propertiesbar" class="col-3 pt-3 pb-3">
                @yield('propertiesbar')
            </div>
        </div>
    </div>
</div>
@yield('scripts')
@stack('scripts')
</body>
</html>
