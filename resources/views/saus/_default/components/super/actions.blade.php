<a class="p-2" href="/cms/{{$moduleName}}/{{$entity->id}}/EDIT"><i class="fas fa-edit"></i></a>
{!! Form::open(['route' => [$callableDelete, $entity->id], 'method' => 'POST', 'class' => 'inline-form']) !!}
{{ Form::hidden('_method', 'DELETE') }}
{{ Form::button(' <i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'link-btn')) }}
{!! Form::close() !!}
