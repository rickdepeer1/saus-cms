@php
    $random = Str::random(6);
@endphp
<div class="row settings-row">
    <div class="col-11">
        <fieldset disabled id="{{ $random }}">

            <div class="row">
                <div class="col-6">
                    <label>key </label>
                    <input class="form-control setting-key" id="key-{{ $random }}" type="text" value="{{ $setting->key }}" data-original="{{ $setting->key }}">
                </div>
                <div class="col-6">
                    <label>value</label>
                    <input class="form-control setting-value" id="value-{{ $random }}" type="text" value="{{ $setting->value }}" data-original="{{ $setting->value }}">
                </div>
            </div>
        </fieldset>

    </div>
    <div class="col-1 pt-3">
        <a class="btn btn-success mt-3 setting-save" style="display: none;" id="save-{{ $random }}" data-id="{{ $setting->id }}" data-module="{{ $moduleName }}" data-link="{{ $random }}"><i class="fas fa-save"></i></a>
        <a class="btn btn-primary mt-3 setting-edit text-white" id="edit-{{ $random }}" data-id="{{ $setting->id }}" data-module="{{ $moduleName }}" data-link="{{ $random }}"><i class="fas fa-edit"></i></a>
    </div>
</div>