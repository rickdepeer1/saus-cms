@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1 class="d-inline-block">{{ ucfirst($moduleName) }} Settings</h1>
    <a class="btn btn-danger text-white float-right mt-2 mr-3"><i class="fas fa-trash"></i></a><a class="btn btn-primary setting-add text-white float-right mt-2 mr-3" data-module="{{ $moduleName }}"> Add setting <i class="fas fa-plus"></i></a>
    @if($settings->count() > 0)
        <div class="settings-wrapper">
            @foreach($settings as $setting)
                @include("saus._default.components.super.setting")
            @endforeach
        </div>
    @else
        <p>No settings yet</p>
    @endif
@endsection

@section('propertiesbar')
@endsection

@section("scripts")
@endsection