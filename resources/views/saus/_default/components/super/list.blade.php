@if(count($entities) > 0)
    <table class="table table-striped mt-3">
        <tr>
            @foreach($columns as $column => $columnName)
            <th>{{ $columnName }}</th>
            @endforeach
            @php
                if(isset($columns['actionButtons'])) {
                    unset($columns['actionButtons']);
                }
            @endphp
        </tr>
        @foreach($entities as $entity)
            <tr>
                @foreach($columns as $column => $columnName)
                    @if(in_array($column, $children) && config()->has('modules.'.$column))
                        <td><a href="{{ route("cms." . config()->get('modules.' . $column . '.moduleName') . ".list") }}">View {{ $columnName }}</a></td>
                        @continue
                    @endif
                    <td>{{ $entity->$column }}</td>
                @endforeach
                <td>{!! $entity->getActionButtons($callableDelete, $moduleName) !!}</td>
            </tr>
        @endforeach
    </table>
    {{$entities->links()}}
@else
    <p>No Entities found</p>
@endif
