<nav class="navbar navbar-dark bg-dark flex-md-nowrap p-0 shadow rounded-0">
    <a class="navbar-brand col-sm-2 col-md-2 mr-0" href="#">Saus CMS <span class="sub-brand">Saus maakt alles beter!</span></a>
    <input class="form-control form-control-dark w-100 rounded-0 col-7" type="text" placeholder="Search" aria-label="Search">
    <a href="/" class="globe-link pl-3"><i class="fas fa-globe"></i></a>
    <ul class="navbar-nav ml-auto mr-3">
        <!-- Authentication Links -->
        @guest
            <li class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            <li class="nav-item">
                @if (Route::has('register'))
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                @endif
            </li>
        @else
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <i class="fas fa-user mr-2"></i> {{ Auth::user()->name }} <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a  class="dropdown-item" href="/dashboard">Dashboard</a>
                    <a  class="dropdown-item" href="/user/settings">Account Settings</a>

                    <a class="dropdown-item" href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
                </div>
            </li>
        @endguest
    </ul>
</nav>