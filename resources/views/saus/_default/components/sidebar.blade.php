<div class="sidebar-sticky">

    <ul class="nav flex-column">
        @foreach($servicesConfig as $serviceName => $serviceConfig)
            <li class="nav-item">
                <a class="nav-link active border-bottom" href="{{ route("cms.{$serviceConfig['moduleName']}.list") }}">
                    <i class="fas fa-home"></i>
                    {{ ucfirst($serviceConfig['moduleName'])  }}
                </a>
            </li>
        @endforeach
        @php /*
        <li class="nav-item">
            <a class="nav-link active border-bottom" href="/cms">
                <i class="fas fa-home"></i>
                Dashboard <span class="sr-only">(current)</span>
            </a>
        </li>
        <li class="nav-item border-bottom">
            <a class="nav-link">
                <i class="fas fa-columns"></i>
                Pages
            </a>
        </li>
        <li class="nav-item border-bottom">
            <a class="nav-link">
                <i class="fas fa-newspaper"></i>
                Articles
            </a>
        </li>
    </ul>
    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Webshop</span>
        <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="plus-circle"></span>
        </a>
    </h6>
    <ul class="nav flex-column">
        <li class="nav-item border-bottom">
            <a class="nav-link">
                <i class="fas fa-shopping-cart"></i>
                Products
            </a>
        </li>
        <li class="nav-item border-bottom">
            <a class="nav-link">
                <i class="fas fa-file"></i>
                Orders
            </a>
        </li>
    </ul>
    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Administration</span>
        <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="plus-circle"></span>
        </a>
    </h6>
    <ul class="nav flex-column">
        <li class="nav-item border-bottom">
            <a class="nav-link">
                <i class="fas fa-users"></i>
                Users
            </a>
        </li>
        <li class="nav-item border-bottom">
            <a class="nav-link" href="{{ route('cms.schedule.list') }}">
                <i class="fas fa-calendar"></i>
                Schedules
            </a>
        </li>
    </ul>
    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>Sidebar heading</span>
        <a class="d-flex align-items-center text-muted" href="#">
            <span data-feather="plus-circle"></span>
        </a>
    </h6>
    <ul class="nav flex-column mb-2">
        <li class="nav-item border-bottom">
            <a class="nav-link" href="#">
                <span data-feather="file-text"></span>
                Link
            </a>
        </li>
    </ul> */
    @endphp
    <div id="company" class="font-weight-light font-italic">
        Development by <a href="#">Saus Media Group</a> &copy;<br>
        Founded by Rick Fransen
    </div>
</div>