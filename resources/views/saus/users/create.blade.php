@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1>Create Product</h1>
    {!! Form::open(['action' => 'ProductsController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('product', 'Product Title')}}
            {{Form::text('product', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        <div class="form-group">
            {{Form::label('description', 'Description')}}
            {{Form::textarea('description', '', ['id' => 'ckeditor', 'class' => 'form-control', 'placeholder' => 'Product description'])}}
        </div>
        <div class="form-group">
            {{Form::label('price', 'Price')}}
            {{Form::text('price', '0.0', ['class' => 'form-control', 'placeholder' => '0.0'])}}
        </div>
        <div class="form-group">
            {{Form::label('stock', 'Stock')}}
            {{Form::text('stock', '0', ['class' => 'form-control', 'placeholder' => '0'])}}
        </div>
        <div class="form-group">
            {{Form::label('sold', 'Sold')}}
            {{Form::text('sold', '0', ['class' => 'form-control', 'placeholder' => '0'])}}
        </div>
        <a href="/cms/products" class="btn btn-default">Back</a>
        {{Form::submit('Submit', ['class' => 'btn btn-secondary float-right'])}}
    {!! Form::close() !!}
@endsection
