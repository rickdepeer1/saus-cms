@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1>Users</h1>
    <a href="/cms/users/create" class="btn btn-secondary">Create</a>
    @if(count($users) > 0)
        <table class="table table-striped mt-3">
            <tr>
                <th>#</th>
                <th>Name</th>
                <th>Email</th>
                <th></th>
            </tr>
        @foreach($users as $user)
            <tr>
                <td>{{$user->id}}</td>
                <td>{{$user->name}}<br/><small>Created on {{$user->created_at}}</small></td>
                <td>{{ $user->email }}</td>
                <td><a class="p-2" href="/cms/users/{{$user->id}}/edit"><i class="fas fa-edit"></i></a>
                    {!! Form::open(['action' => ['UsersController@destroy', $user->id], 'method' => 'POST', 'class' => 'inline-form']) !!}
                    {{Form::hidden('_method', 'DELETE')}}
                    {{Form::button(' <i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'link-btn'))}}
                    {!! Form::close() !!}</td>
            </tr>
        @endforeach
        </table>
        {{$users->links()}}
    @else
        <p>No Users found</p>
    @endif
@endsection