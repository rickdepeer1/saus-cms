@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1>Tasks</h1>
    <a href="{{route("cms.task.create")}}" class="btn btn-secondary">Create</a>
    {!! $list !!}
@endsection