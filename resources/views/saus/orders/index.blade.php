@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1>Orders</h1>
    <a href="/cms/orders/create" class="btn btn-secondary">Create</a>
    @if(count($orders) > 0)
        <table class="table table-striped mt-3">
            <tr>
                <th>#</th>
                <th>Date</th>
                <th>Shipping Date</th>
                <th>Status</th>
                <th>Comments</th>
                <th>Customer</th>
                <th>Edit</th>
            </tr>
        @foreach($orders as $order)
                <tr>
                    <td><b><a href="/cms/products/{{$order->id}}">{{$order->id}}</a></td>
                    <td>{{$order->created_at}}</td>
                    <td>{{$order->Status}}</td>
                    <td>{!! $order->comments !!}</td>
                    <td>{{$product->custormerID}}</td>
                    <td><a class="p-2" href="/cms/orders/{{$order->id}}/edit"><i class="fas fa-edit"></i></a>
                        {!! Form::open(['action' => ['OrdersController@destroy', $product->id], 'method' => 'POST', 'class' => 'inline-form']) !!}
                        {{Form::hidden('_method', 'DELETE')}}
                        {{Form::button(' <i class="fa fa-trash"></i>', array('type' => 'submit', 'class' => 'link-btn'))}}
                        {!! Form::close() !!}</td>
                </tr>

        @endforeach
        </table>
        {{$orders->links()}}
    @else
        <p>No Orders found</p>
    @endif
@endsection