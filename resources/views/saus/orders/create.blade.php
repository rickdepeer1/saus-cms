@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1>Create Order</h1>
    {!! Form::open(['action' => 'OrdersController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            <h2>Order details</h2>
            @if(count($products) > 0)
                <table class="table table-striped mt-3">
                    <tr>
                        <th>#</th>
                        <th>Product</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Stock</th>
                        <th>Sold</th>
                        <th>Add</th>
                    </tr>
                    @foreach($products as $product)
                        <tr>
                            <td>{{$product->id}}</td>
                            <td><b><a href="/cms/products/{{$product->id}}">{{$product->product}}</a><br/><small>Created on {{$product->created_at}}</small></b></td>
                            <td>{!! $product->description !!}</td>
                            <td>&#8364; {{number_format($product->price, 2, '.', '')}}</td>
                            <td>{{$product->stock}}</td>
                            <td>{{$product->sold}}</td>
                            <td><input type="number" style="width: 70px; display: inline-block" class="form-control" name="quantity" value="1" placeholder="1"><a class="p-2" href="#"><i class="fas fa-plus"></i></a></td>
                        </tr>
                    @endforeach
                </table>
                {{$products->links()}}
            @else
                <p>No Products found</p>
            @endif
        </div>
        <hr>
        <div class="form-group">
            <h2>Order Summary</h2>
            <div class="order-summary">
                Order is empty
            </div>
            {{Form::hidden('orderDetails', '', ['id' => 'orderDetails'])}}
        </div>
        <hr>
        <div class="form-group">
            {{Form::label('shippingDate', 'Shipping date')}}
            {{Form::date('shippingDate', '', ['class' => 'form-control'])}}
        </div>
        <div class="form-group">
            {{Form::label('comments', ' Order comments')}}
            {{Form::textarea('comments', '', [ 'class' => 'form-control', 'placeholder' => 'Order Comments'])}}
        </div>
        <hr>
        <a href="/cms/orders" class="btn btn-default">Back</a>
        {{Form::submit('Submit', ['class' => 'btn btn-secondary float-right'])}}
    {!! Form::close() !!}
@endsection
