@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1>Schedules</h1>
    <a href="{{route("cms.schedule.create")}}" class="btn btn-secondary">Create</a>
    {!! $list !!}
@endsection

@section('propertiesbar')
    <ul class="list-group">
        <li class="list-group-item"><a href="{{ route("cms.schedule.settings") }}">Settings<i class="fas fa-cogs float-right"></i></li>
        <li class="list-group-item">Dapibus ac facilisis in</li>
        <li class="list-group-item">Morbi leo risus</li>
        <li class="list-group-item">Porta ac consectetur ac</li>
        <li class="list-group-item">Vestibulum at eros</li>
    </ul>
@endsection
