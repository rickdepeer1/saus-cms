@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1>Create Schedule</h1>
    <a href="{{route("cms.schedule.list")}}" class="btn btn-default">Back</a>
    {{ Form::submit('Submit', ['class' => 'btn btn-secondary float-right mb-3']) }}

    <hr>
    {!! Form::open(['route' => [$callableStore, $entity->id], 'method' => 'POST']) !!}
    <div class="form-group">
        {{Form::label('schedule', 'Schedule')}}
        {{Form::text('schedule', $entity->schedule, ['class' => 'form-control', 'placeholder' => 'Title'])}}
    </div>
    <div class="form-group">
        {{Form::label('description', 'Description')}}
        {{Form::textarea('description', $entity->description, ['id' => 'ckeditor', 'class' => 'form-control', 'placeholder' => 'Schedule description'])}}
    </div>
    <hr>
    <a href="/cms/schedules" class="btn btn-default">Back</a>
    {{Form::submit('Submit', ['class' => 'btn btn-secondary float-right mb-3'])}}
    {!! Form::close() !!}
@endsection

@section('propertiesbar')
    <div class="card">
        <div class="card-body p-0">
            <table id="tasks-table" class="table mb-0">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Task</th>
                    <th scope="col">Priority</th>
                    <th scope="col"></th>
                    <th scope="col"></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <th scope="row">1</th>
                    <td></td>
                    <td></td>
                    <td><i class="fas fa-edit pr-2"></i><i class="fas fa-minus-circle"></i></td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/schedule.script.js') }}" defer></script>
@endsection