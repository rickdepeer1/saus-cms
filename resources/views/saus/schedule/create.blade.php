@extends(env("SAUS_LAYOUT"))

@section('content')
    <h1>Create Schedule</h1>
    <a href="/cms/schedules" class="btn btn-default">Back</a>
    {{Form::submit('Submit', ['class' => 'btn btn-secondary float-right mb-3'])}}
    <hr>
    {!! Form::open(['action' => 'SchedulesController@store', 'method' => 'POST']) !!}
        <div class="form-group">
            {{Form::label('schedule', 'Schedule Title')}}
            {{Form::text('schedule', '', ['class' => 'form-control', 'placeholder' => 'Title'])}}
        </div>
        <div class="form-group">
            {{Form::label('description', 'Description')}}
            {{Form::textarea('description', '', ['id' => 'ckeditor', 'class' => 'form-control', 'placeholder' => 'Task description'])}}
        </div>
        <hr>
        <b>Add a task</b>
        <input type="hidden" value="0" id="task-id-input">
        <div class="row">
            <div class="form-group col-10">
                {{Form::label('task', 'Task')}}
                <input type="text" id="task-input" class="form-control" placeholder="Task">
            </div>
            <div class="form-group col-2">
                {{Form::label('priority', 'Priority')}}
                <input type="number" value="1" id="priority-input" class="form-control" placeholder="1">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-6">
                {{Form::label('expectedHours', 'Expected hours')}}
                <input type="number" value="1" id="expectedHours-input" class="form-control" placeholder="0">

            </div>
            <div class="form-group col-6">
                {{Form::label('deadline', 'Deadline')}}
                <input type="datetime-local" value="{{ \Carbon\Carbon::now() }}" id="datetime-input" class="form-control">
            </div>
        </div>
        <a id="add-task" class="btn btn-success float-right">Add task</a><br><br>
        <hr>
        <a href="/cms/schedules" class="btn btn-default">Back</a>
        {{Form::hidden('tasks', '', ['id' => 'tasks'])}}
        {{Form::submit('Submit', ['class' => 'btn btn-secondary float-right mb-3'])}}
    {!! Form::close() !!}
@endsection

@section('propertiesbar')
    <div class="card">
        <div class="card-body p-0">
            <table id="tasks-table" class="table mb-0">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Task</th>
                        <th scope="col">Priority</th>
                        <th scope="col"></th>
                        <th scope="col"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="row">1</th>
                        <td></td>
                        <td></td>
                        <td><i class="fas fa-edit pr-2"></i><i class="fas fa-minus-circle"></i></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
@endsection

@section('scripts')
    <script src="{{ asset('js/schedule.script.js') }}" defer></script>
@endsection