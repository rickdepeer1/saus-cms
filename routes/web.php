<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*
|--------------------------------------------------------------------------
| Custom Routes
|--------------------------------------------------------------------------
*/

use App\Http\Controllers\SchedulesService;
use App\Schedule;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;




Route::group(['prefix' => 'cms', 'as' => 'cms.'], function () {
    $servicesConfig = Config::get('modules');
    foreach($servicesConfig as $serviceName => $serviceConfig){
        Route::group(['prefix' => $serviceConfig['moduleName'], 'as' => $serviceConfig['moduleName'].'.'], function () use ($serviceName, $serviceConfig) {

            Route::get('/', function() use ($serviceName){

                return app()->make($serviceName)->list();
            })->name('list');



            Route::get('/{id}/SHOW', function($id) use ($serviceName){

                return app()->make($serviceName)->show($id);
            })->name('show');



            Route::get('/CREATE', function() use ($serviceName){

                return app()->make($serviceName)->create();
            })->name('create');



            Route::get('/{id}/EDIT', function($id) use ($serviceName){

                return app()->make($serviceName)->edit($id);
            })->name('edit');



            Route::delete('/{id}/DELETE', function($id) use ($serviceName){

                return app()->make($serviceName)->destroy($id);
            })->name('delete');



            Route::post('/STORE', function(Request $request) use ($serviceName){
                return app()->make($serviceName)->store($request, 0);
            })->name('store');

            Route::post('/{id}/UPDATE', function($id, Request $request) use ($serviceName){
                return app()->make($serviceName)->store($request, $id);
            })->name('update');

            Route::get('/settings', function() use ($serviceName){
                return app()->make($serviceName)->settings();
            })->name('settings');

            Route::post('/setting/STORE', function(Request $request) use ($serviceName){
                return app()->make($serviceName)->settingStore($request);
            })->name('setting.store');

            Route::get('/setting/ADD', function() use ($serviceName){
                return app()->make($serviceName)->settingAdd();
            })->name('setting.add');
        });
    }
});


/*
|--------------------------------------------------------------------------
| Resource Routes
|--------------------------------------------------------------------------
*/


/*
|--------------------------------------------------------------------------
| Auth Routes
|--------------------------------------------------------------------------
*/


// Authentication Routes...
Route::get('/cms/login', '\App\Core\Controllers\Auth\LoginController@showLoginForm')->name('login');
Route::post('/cms/login', '\App\Core\Controllers\Auth\LoginController@login');
Route::post('/cms/logout', '\App\Core\Controllers\Auth\LoginController@logout')->name('logout');

// Registration Routes...
Route::get('/cms/register', '\App\Core\Controllers\Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('/cms/register', '\App\Core\Controllers\Auth\RegisterController@register');

// Password Reset Routes...
Route::get('/cms/password/reset', '\App\Core\Controllers\Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('/cms/password/email', '\App\Core\Controllers\Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('/cms/password/reset/{token}', '\App\Core\Controllers\Auth\ResetPasswordController@showResetForm');
Route::post('/cms/password/reset', '\App\Core\Controllers\Auth\ResetPasswordController@reset');
