/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/schedule.script.js":
/*!*****************************************!*\
  !*** ./resources/js/schedule.script.js ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(document).ready(function () {
  console.log('---- Script loaded ----');
  var tasks = JSON.parse($("#tasks").val());
  UpdateTasks(tasks);
  /****************************************************
   *
   *      All jQuery events in sequence to reduce $ calls
   *
   ***************************************************/

  $(document.body).on('click', '#add-task', function () {
    // retrieve fata form hidden input field to generate display for user
    var id = $('#task-id-input').val();
    var taskTitle = $('#task-input').val();
    var expectedHours = $('#expectedHours-input').val();
    var datetime = $('#datetime-input').val();
    var priority = $('#priority-input').val();
    tasks = JSON.parse($("#tasks").val()); //set id for new entry

    if (parseInt(id) === 0) {
      id = tasks.length + 1;
    } //Create task as json, push to all tasks and send to update func


    var task = {
      "id": parseInt(id),
      "task": taskTitle,
      "priority": parseInt(priority),
      "expectedHours": parseInt(expectedHours),
      "datetime": datetime
    };
    tasks.push(task);
    UpdateTasks(tasks);
  }).on('click', '#task-edit', function (e) {
    var tasks = JSON.parse($("#tasks").val());
    var id = parseInt($(e.currentTarget).attr('data-id'));
  }).on('click', '#task-remove', function (e) {
    var tasks = JSON.parse($("#tasks").val());
    var id = parseInt($(e.currentTarget).attr('data-id'));

    for (var task in tasks) {
      if (id === parseInt(task) + 1) {
        //tasks.slice(parseInt(task), 1);
        remove(tasks, parseInt(task));
        console.log(task);
      }
    }

    console.log(tasks);
    UpdateTasks(tasks);
  });
});

function UpdateTasks(tasks) {
  var template = '';
  var priority = 1;

  for (var task in tasks) {
    template += "<tr><th scope='row'>" + (parseInt(task) + 1) + "</th><td>" + tasks[task].task + "</td><td>" + tasks[task].priority + "</td><td><a href='#' class='pl-3' id='task-up' data-id='\"+(parseInt(task)+1)+\"'><i class='fas fa-caret-up pr-2'></i></a><a href='#' id='task-down' data-id='\"+(parseInt(task)+1)+\"'><i class='fas fa-caret-down pr-2'></i></a></td><td><a href='#' id='task-edit' data-id='" + (parseInt(task) + 1) + "'><i class='fas fa-edit pr-2'></i></a><a href='#' id='task-remove' data-id='" + (parseInt(task) + 1) + "'><i class='fas fa-minus-circle'></i></a></td></tr>";
    priority = parseInt(task) + 2;
  }

  $('#priority-input').val(priority);
  $('#tasks-table tbody').html(template);
  $("#tasks").val(JSON.stringify(tasks));
  console.log('---- Tasks Updated ----');
}

function remove(array, element) {
  array.splice(element, 1);
}

/***/ }),

/***/ 1:
/*!***********************************************!*\
  !*** multi ./resources/js/schedule.script.js ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\homestead\webapp\resources\js\schedule.script.js */"./resources/js/schedule.script.js");


/***/ })

/******/ });