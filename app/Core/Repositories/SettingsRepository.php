<?php namespace App\Core\Repositories;

use App\Core\Models\Setting;

/**
 * Class SettingsRepository.
 */
class SettingsRepository extends Repository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Setting::class;
    }

    public function getSettingByKey(string $key, string $moduleName = "global"){
        return Setting::where("key", $key)->where("module_name", $moduleName)->get();
    }

    public function getSettingByValue($value, string $moduleName = "global"){
        return Setting::where("value", $value)->where("module_name", $moduleName)->get();
    }

    public function get(string $key){
        return $this->getSettingByKey($key);
    }
}
