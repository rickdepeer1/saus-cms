<?php

namespace App\Core\Services\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class CrudServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $servicesConfig = Config::get('modules');
        foreach($servicesConfig as $serviceName => $serviceConfig){
            //$this->app->singleton($serviceConfig);

            $this->app->bind($serviceName, function() use ($serviceName, $serviceConfig){

                $children = isset($serviceConfig['children']) ? $serviceConfig['children'] : [];
                $scheduleService = new $serviceConfig['service']($serviceConfig['model'], $serviceConfig['moduleName'], $children);
                $scheduleService->setColumnsToDisplay($serviceConfig['columnsToDisplay']);
                return  $scheduleService;
            });
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function($view){
            $servicesConfig = Config::get('modules');

            return $view->with('servicesConfig', $servicesConfig);
        });
    }
}
