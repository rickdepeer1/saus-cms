<?php namespace App\Core\Models;

use Illuminate\Database\Eloquent\Model;

class CrudModel extends Model
{

    public function getActionButtons(string $callableDelete, string $moduleName){
        return view('saus._default.components.super.actions')
            ->with('entity', $this)
            ->with('callableDelete', $callableDelete)
            ->with('moduleName', $moduleName);
    }

}