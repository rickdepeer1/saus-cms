<?php namespace App\Core\Controllers;


class FormController
{

    public function recaptcha(){
        return view('saus._default.components.super.recatcha');

    }

    public function text(){
        return view('saus._default.components.super.text');

    }

    public function password(){
        return view('saus._default.components.super.password');

    }

    public function textarea(){
        return view('saus._default.components.super.textarea');
    }

}