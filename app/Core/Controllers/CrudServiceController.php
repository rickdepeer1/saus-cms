<?php

namespace App\Core\Controllers;

use App\Core\Controllers\CrudComponentController;
use App\Core\Models\Setting;
use Hamcrest\Core\Set;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

/**
 * Class CrudComponentController
 * @package App\Http\Controllers
 */
class CrudServiceController extends Controller
{
    /**
     * @var \Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * @var string
     */
    private $callableDelete;
    /**
     * @var array
     */
    private $showColumns = [];
    /**
     * @var string
     */
    public $moduleName;


    private $crudComponentController;

    /**
     * @var string
     */
    private $callableStore;

    /**
     * @var string
     */
    private $callableUpdate;
    /**
     * @var array
     */
    private $children;

    /**
     * CrudComponentController constructor.
     * @param string $module
     * @param string $moduleName
     * @param string $callableDelete
     */
    public function __construct(string $model, string $moduleName, array $children = [])
    {
        $this->model = $model;
        $this->children = $children;
        $this->callableDelete = "cms.{$moduleName}.delete";
        $this->callableStore = "cms.{$moduleName}.store";
        $this->callableUpdate = "cms.{$moduleName}.update";
        $this->moduleName = $moduleName;

        $crud = new CrudComponentController("LIST", $model, $moduleName);
        $crud->setCallableDelete($this->callableDelete);

        $this->crudComponentController = $crud;
    }

    /**
     * @param array $showColumns
     */
    public function setColumnsToDisplay(array $showColumns){
        $this->crudComponentController->setColumnsToDisplay($showColumns);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list()
    {
        return view("saus.{$this->moduleName}.list")->with('list', $this->crudComponentController->getList($this->children));
    }

    /**
     *
     */
    public function create()
    {
        return view("saus.{$this->moduleName}.form")->with('entity', new $this->model)->with('callableStore', $this->callableStore)->with('moduleName', $this->moduleName);
    }

    /**
     * @param $id
*/
    public function show($id)
    {

    }

    public function settings(){
        $settings = Setting::where('module_name', '=', $this->moduleName)->get();
        return View::first(["saus.{$this->moduleName}.settings", "saus._default.components.super.settings"])->with('moduleName', $this->moduleName)->with('settings', $settings);
    }

    public function settingAdd(){
        $setting = new Setting();
        $setting->id = 0;
        return View::first(["saus.{$this->moduleName}.setting", "saus._default.components.super.setting"])->with('moduleName', $this->moduleName)->with('setting', $setting);
    }

    public function settingStore(Request $request){
        $id = (int)$request->input('id');
        $setting = ($id === 0 ? new Setting() : Setting::find($id));

        $setting->key = $request->input('key');
        $setting->value = $request->input('value');
        $setting->module_name = $this->moduleName;
        $setting->save();

        $request->session()->flash('status', 'Task was successful!');
        return \json_encode(['redirect' => route("cms.{$this->moduleName}.settings")]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $entity = $this->model::find($id);
        return view("saus.{$this->moduleName}.form")->with('entity', $entity)->with('callableStore', $this->callableUpdate)->with('moduleName', $this->moduleName);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        $this->model::find($id)->delete();
        return redirect("/cms/{$this->moduleName}")->with('success', "{$this->moduleName} Removed");
    }
}
