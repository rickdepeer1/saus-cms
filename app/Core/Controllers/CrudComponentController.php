<?php namespace App\Core\Controllers;

use Illuminate\Support\Facades\App;

/**
 * Class CrudComponentRepository
 * @package App\Http\Repositories
 */
class CrudComponentController extends Controller
{
    /**
     * @var string
     */
    public $type;
    /**
     * @var
     */
    private $showColumns;
    /**
     * @var
     */
    private $hideColumns;
    /**
     * @var
     */
    private $entities;
    /**
     * @var string
     */
    private $callableDelete = 'CrudComponentController@destroy';
    /**
     * @var
     */
    private $moduleName;

    /**
     * CrudComponentRepository constructor.
     * @param string $action
     * @param        $model
     * @param        $moduleName
     */
    public function __construct(string $action, $model, $moduleName)
    {
        $this->type = $action;
        $this->entities = $model::orderBy('id', 'asc')->paginate(10);
        $this->moduleName = $moduleName;
    }

    /**
     * @return array|string
     */
    public function getComponents()
    {
        $components = [];
        switch($this->type){
            case "LIST":
                $components["list"] = view('saus._default.components.super.list')->with('columns', $this->showColumns)->with('entities', $this->entities);
                return $components;
                break;
            case "CREATE":
                return 'create';
            case "EDIT":
                return "edit";
            case "DELETE":
                return "delete";
            default:
                return App::abort(404);
                break;
        }
    }

    /**
     * @param array $columns
     */
    public function setColumnsToDisplay(array $columns)
    {
        $columnsSanitized = array();
        foreach($columns as $column => $columnName)
        {
            if(is_numeric($column)){
                $columnsSanitized[$columnName] = ucfirst($columnName);
            }elseif(empty($columnName)){
                $columnsSanitized[$column] = '';
            }else{
               $columnsSanitized[$column] = $columnName;
            }
        }
        $this->showColumns = $columnsSanitized;
    }

    /**
     * @param array $columns
     */
    public function setColumnsToHide(array $columns)
    {
        $this->hideColumns = $columns;
    }

    /**
     * @return mixed
     */
    public function getList(array $children)
    {
        return view('saus._default.components.super.list')
            ->with('columns', $this->showColumns)
            ->with('entities', $this->entities)
            ->with('callableDelete', $this->callableDelete)
            ->with('moduleName', $this->moduleName)
            ->with('children', $children);
    }

    /**
     * @return mixed
     */
    public function getEdit()
    {
        $formFields = view("saus.{$this->moduleName}.form-fields");
        $propertyFields = view("saus.{$this->moduleName}.property-fields");

        return view("saus.{$this->moduleName}.form")->with('formFields', $formFields)->with('propertyFields', $propertyFields);
    }

    /**
     * @param string $callableDelete
     */
    public function setCallableDelete(string $callableDelete)
    {
        $this->callableDelete = $callableDelete;
    }
}
