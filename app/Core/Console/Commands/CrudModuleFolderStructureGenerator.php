<?php

namespace App\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class CrudModuleFolderStructureGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:CrudModuleFolderStructure {moduleName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This generates the necessary folder structure for a new module';


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $moduleName = $this->argument('moduleName');
        if(File::exists('app/Cms/' . ucfirst($moduleName))){
            $this->line("<info>Module already exists:</info> $moduleName");
            return;
        }

        File::makeDirectory('app/Cms/' . ucfirst($moduleName));
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Controllers');
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Models');
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Repositories');
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Repositories/Interfaces');
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Services');
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Services/Providers');
        $this->line("<info>Created Module Folder Structure:</info> $moduleName");
    }

}
