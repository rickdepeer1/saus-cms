<?php

namespace App\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class CrudModuleGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:CrudModuleModel {moduleName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This executes the necessary commands for creating a new module';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $moduleName =$this->argument('moduleName');

        Artisan::call('make:CrudModuleFolderStructureGenerator', [$moduleName]);
        Artisan::call('make:CrudModuleServiceGenerator', [$moduleName]);
        Artisan::call('make:CrudModuleModelGenerator', [$moduleName]);
        Artisan::call('make:CrudModuleRepositoryGenerator', [$moduleName]);

        $this->line("<info>Created Module :</info> $moduleName");
    }
}
