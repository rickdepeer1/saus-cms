<?php

namespace App\Core\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Facades\File;

class CrudModuleRepositoryGenerator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:CrudModuleRepository {moduleName}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This generates the necessary files and file structure for a new module';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $moduleName =$this->argument('moduleName');
        if(File::exists('app/Cms/' . ucfirst($moduleName))){
            $this->line("<info>Module already exists:</info> $moduleName");
            return;
        }


        $path = $this->getPath($this->repositoryClass);
        $this->line("<info>Module:</info> $path");


        File::makeDirectory('app/Cms/' . ucfirst($moduleName));
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Controllers');
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Models');
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Repositories');
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Repositories/Interfaces');
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Services');
        File::makeDirectory('app/Cms/' . ucfirst($moduleName) . '/Services/Providers');
        $this->line("<info>Created Module :</info> $moduleName");
    }

    /**
     * Get the stub file for the generator.
     *
     * @return string
     */
    protected function getStub()
    {
        return  base_path('app/Core/Console/Commands/Dummies/Repositories/Repository.stub');
    }
}
