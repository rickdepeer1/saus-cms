<?php

namespace App\Cms\Schedule\Services;

use App\Cms\Schedule\Models\Model;
use App\Cms\Schedule\Models\Task;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ScheduleService extends \App\Core\Controllers\CrudServiceController
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(
        Request $request,
        int $id = 0
    ){
        $this->validate($request, [
            'schedule' => 'required',
            'tasks' => 'required',
        ]);

        $schedule = ($id === 0 ? new Model() : $this->model::find($id));

        $schedule->schedule = $request->input('schedule');
        $schedule->description = $request->input('description');
        $schedule->save();

        return redirect("/cms/{$this->moduleName}")->with('success', ucfirst($this->moduleName).' Stored');
    }
}
