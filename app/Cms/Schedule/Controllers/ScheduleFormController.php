<?php namespace App\Schedule\Controllers;

use App\Core\Controllers\FormController;

class ScheduleFormController extends FormController
{
    public function scheduleText(){
        return view('saus.schedule.text');
    }
}