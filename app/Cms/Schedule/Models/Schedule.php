<?php namespace App\Cms\Schedule\Models;

use App\Core\Models\CrudModel;

class Schedule extends CrudModel
{
    public function task()
    {
        return $this->hasMany('App\Cms\Schedule\Models\Task');
    }
}
