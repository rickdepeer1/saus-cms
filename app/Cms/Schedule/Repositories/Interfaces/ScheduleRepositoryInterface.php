<?php namespace App\Cms\Schedule\Interfaces\Repositories;

interface ScheduleRepositoryInterface
{
    /**
     * Get's a schedule by it's ID
     *
     * @param int
     */
    public function get($schedule_id);

    /**
     * Get's all Schedules.
     *
     * @return mixed
     */
    public function all();

    /**
     * Deletes a schedule.
     *
     * @param int
     */
    public function delete($schedule_id);

    /**
     * Updates a schedule.
     *
     * @param int
     * @param array
     */
    public function update($schedule_id, array $schedule_data);
}