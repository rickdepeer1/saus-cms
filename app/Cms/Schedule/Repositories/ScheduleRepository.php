<?php namespace App\Cms\Schedule\Repositories;

use App\Cms\Schedule\Interfaces\Repositories\ScheduleRepositoryInterface;
use App\Cms\Schedule\Models\Schedule as Model;
use App\Core\Repositories\Repository;

class ScheduleRepository extends Repository implements ScheduleRepositoryInterface
{

    /**
     * Get's a Schedules by it's ID
     *
     * @param int
     * @return
     */
    public function get($id)
    {
        return Model::find($id);
    }

    /**
     * Get's all Schedules.
     *
     * @return mixed
     */
    public function all()
    {
        $posts = Model::orderBy('created_at', 'desc');
        return $posts;
    }

    /**
     * Deletes a Schedules.
     *
     * @param int
     * @return
     */
    public function delete($id)
    {
        $post = Model::find($id);
        $post->delete();

        return $post;
    }

    /**
     * Updates a Schedules.
     *
     * @param int
     * @param array
     */
    public function update($id, array $data)
    {
        $post = Model::find($id);
        foreach($data as $key => $value){
            $post->$$key = $value;
        }
        $post->save();
    }
}