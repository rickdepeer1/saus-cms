# Saus Content Management System
### Implementing Crud Service Package

This system is build with the idea of building upon Laravel to add a core Crud service. 
This service is going to provide an easy way of creating modules/domains/entities with 
instant Create Read Update Delete functionality while trying to keep customization as 
open and easy as possible. This service will be able to provide components as table lists
of entities, will be able to provide customizable forms and will route it all in a standardized manner

###Feature todo list:

- automatic/dynamic route registry
- recursive modules/entities
- dynamic and automatic post storing (as of now you need store function in your module service)
- single config file for registering modules
- dynamic loaded menu
- settings page for every module (including form field creations)
- artisan console command for creating module
- implement api 
- implement vue
- convert project to package
- search
- filters
- sorting